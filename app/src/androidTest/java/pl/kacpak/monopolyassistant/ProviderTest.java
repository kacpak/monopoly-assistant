package pl.kacpak.monopolyassistant;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.test.AndroidTestCase;
import android.util.Log;

import pl.kacpak.monopolyassistant.data.DataContract;

/**
 * Created by Mateusz on 2014-08-09.
 */
public class ProviderTest extends AndroidTestCase {

    public void testInsertReadProvider() {
        final String LOG_NAME = "testInsertReadProvider";
        Cursor cursor;

        cursor = mContext.getContentResolver().query(
                DataContract.PlayerPropertiesEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        Log.d(LOG_NAME + " playerProperties", DatabaseUtils.dumpCursorToString(cursor));

        cursor = mContext.getContentResolver().query(
                DataContract.PropertyDataEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        Log.d(LOG_NAME + " propertyData", DatabaseUtils.dumpCursorToString(cursor));
    }

}
