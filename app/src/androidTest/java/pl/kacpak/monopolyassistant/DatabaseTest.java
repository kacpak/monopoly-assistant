package pl.kacpak.monopolyassistant;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import pl.kacpak.monopolyassistant.data.DataContract.PlayerPropertiesEntry;
import pl.kacpak.monopolyassistant.data.DataContract.PropertyDataEntry;
import pl.kacpak.monopolyassistant.data.MonopolyDbHelper;

public class DatabaseTest extends AndroidTestCase {

    public void testCreateDb() throws Throwable {
        final String LOG_NAME = "getDataFromDatabase";
        Log.i(LOG_NAME, "Started");

        mContext.deleteDatabase(MonopolyDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new MonopolyDbHelper(mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();

        Log.i(LOG_NAME, "Ended");
    }

    public void testInsertReadDb() {
        final String LOG_NAME = "testInsertReadDb";
        SQLiteDatabase db = new MonopolyDbHelper(mContext).getWritableDatabase();

        // Przygotowuje wartości do wstawienia
        ContentValues propertyDataRow = new ContentValues();
        propertyDataRow.put(PropertyDataEntry._ID, 1);
        propertyDataRow.put(PropertyDataEntry.COLUMN_NAME, "Ulica Konopacka");

        ContentValues playerPropertiesRow = new ContentValues();
        playerPropertiesRow.put(PlayerPropertiesEntry._ID, 1);
        playerPropertiesRow.put(PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER, 3);

        // Wstawiam wartości do tabeli i otrzymuję id wiersza
        long propertyDataRowId = db.insert(PropertyDataEntry.TABLE_NAME, null, propertyDataRow);
        long playerPropertiesRowId = db.insert(PlayerPropertiesEntry.TABLE_NAME, null, playerPropertiesRow);

        // Jeśli wartość wiersza to -1 to znak że coś poszło nie tak
        assertTrue(propertyDataRowId != -1);
        assertTrue(playerPropertiesRowId != -1);

        // Przygotowuje cursor, który pobierze wszystkie dane z tabeli
        Cursor propertyDataCursor = db.query(
                PropertyDataEntry.TABLE_NAME,   // Nazwa tabeli
                null,                           // Kolumny do pobrania; null - wszystkie
                null,                           // Kolumny do zapytanie WHERE
                null,                           // i ich oczekiwane wartości
                null,                           // Kolumny do grupowania
                null,                           // Kolumny do filtrowania po grupach wierszy
                null                            // Kolejność sortowania
        );
        Cursor playerPropertiesCursor = db.query(
                PlayerPropertiesEntry.TABLE_NAME,   // Nazwa tabeli
                null,                           // Kolumny do pobrania; null - wszystkie
                null,                           // Kolumny do zapytanie WHERE
                null,                           // i ich oczekiwane wartości
                null,                           // Kolumny do grupowania
                null,                           // Kolumny do filtrowania po grupach wierszy
                null                            // Kolejność sortowania
        );

        Log.d(LOG_NAME + " propertyData", DatabaseUtils.dumpCursorToString(propertyDataCursor));
        Log.d(LOG_NAME + " playerProperties", DatabaseUtils.dumpCursorToString(playerPropertiesCursor));


        final String MY_QUERY = "SELECT * FROM " + PlayerPropertiesEntry.TABLE_NAME + " NATURAL JOIN " + PropertyDataEntry.TABLE_NAME;
        Cursor cursor = db.rawQuery(MY_QUERY, new String[]{});
        Log.d(LOG_NAME + " myQuery", DatabaseUtils.dumpCursorToString(cursor));

        db.close();
    }
}