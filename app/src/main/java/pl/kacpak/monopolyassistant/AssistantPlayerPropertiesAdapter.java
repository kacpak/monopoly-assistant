package pl.kacpak.monopolyassistant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import pl.kacpak.monopolyassistant.data.GameManager.PlayerProperty;

/**
 * Created by Mateusz on 2014-08-13.
 */
public class AssistantPlayerPropertiesAdapter extends ArrayAdapter<PlayerProperty> {

    private Context mContext;
    private ArrayList<PlayerProperty> mValues;

    public AssistantPlayerPropertiesAdapter(Context context, ArrayList<PlayerProperty> values) {
        super(context, R.layout.list_item_property, values);
        mContext = context;
        mValues = values;

    }

    @Override
    public void addAll(PlayerProperty... items) {
        mValues.addAll(Arrays.asList(items));
    }

    @Override
    public void clear() {
        mValues.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PlayerProperty playerProperty = mValues.get(position);

        View rowView;
        if (playerProperty.isPledged)
            rowView = LayoutInflater.from(mContext).inflate(R.layout.list_item_property_mortgage, parent, false);
        else if (playerProperty.isSpecial2)
            rowView = LayoutInflater.from(mContext).inflate(R.layout.list_item_property_no_detail, parent, false);
        else
            rowView = LayoutInflater.from(mContext).inflate(R.layout.list_item_property, parent, false);

        TextView id = (TextView) rowView.findViewById(R.id.list_item_id);
        id.setText(String.valueOf(playerProperty.id));

        TextView name = (TextView) rowView.findViewById(R.id.list_item_name);
        name.setText(playerProperty.name);

        if (playerProperty.isSpecial2 || playerProperty.isPledged)
            return rowView;

        TextView housesNumber = (TextView) rowView.findViewById(R.id.list_item_houses_number);
        TextView passingPrice = (TextView) rowView.findViewById(R.id.list_item_passing_price);

        String housesNumberTxt;
        if (playerProperty.isSpecial1) {
            housesNumberTxt = null;
        } else switch (playerProperty.housesNumber) {
            case 0: housesNumberTxt = mContext.getString(R.string.list_item_houses_zero); break;
            case 5: housesNumberTxt = mContext.getString(R.string.list_item_houses_hotel); break;
            default: housesNumberTxt = String.format(mContext.getString(R.string.list_item_houses_number), playerProperty.housesNumber); break;
        }

        String passingPriceTxt = String.format(mContext.getString(R.string.assistant_money_format), playerProperty.passingPrice);

        housesNumber.setText(housesNumberTxt);
        passingPrice.setText(passingPriceTxt);

        return rowView;
    }
}
