package pl.kacpak.monopolyassistant;

import android.app.Dialog;
import android.app.DialogFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import pl.kacpak.monopolyassistant.data.DataContract.GameInfoEntry;
import pl.kacpak.monopolyassistant.data.GameManager;

/**
 * Created by Mateusz on 2014-08-15.
 */
public class MainMenuDialogFragmentLoadGame extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu_load_game, container, false);

        final Cursor games = GameManager.getSavedGamesCursor();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),
                android.R.layout.simple_list_item_1,
                games,
                new String[] { GameInfoEntry.COLUMN_NAME },
                new int[] { android.R.id.text1 },
                0);

        ListView listView = (ListView) view.findViewById(R.id.load_game_dialog_listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                games.moveToPosition(position);
                long gameId = games.getInt(games.getColumnIndex(GameInfoEntry._ID));
                ((MainMenuFragment.MainMenuCallback) getActivity()).onGameLoadId(gameId);
                dismiss();
            }
        });


        return view;
    }
}
