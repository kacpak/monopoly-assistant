package pl.kacpak.monopolyassistant.data;

import android.provider.BaseColumns;

/**
 * Created by Mateusz on 2014-08-08.
 */
public class DataContract {

    public static final class PropertyDataEntry implements BaseColumns {
        public static final String TABLE_NAME = "propertyData";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_PURCHASE_PRICE = "purchasePrice";
        public static final String COLUMN_HOUSE_PRICE = "housePrice";
        public static final String COLUMN_HOTEL_PRICE = "hotelPrice";
        public static final String COLUMN_RENT0 = "rent0";
        public static final String COLUMN_RENT1 = "rent1";
        public static final String COLUMN_RENT2 = "rent2";
        public static final String COLUMN_RENT3 = "rent3";
        public static final String COLUMN_RENT4 = "rent4";
        public static final String COLUMN_RENT5 = "rent5";
        public static final String COLUMN_MORTGAGE = "mortgage";
        public static final String COLUMN_SPECIAL1 = "special1";
        public static final String COLUMN_SPECIAL2 = "special2";
        public static final String COLUMN_SPECIAL3 = "special3";
        public static final String COLUMN_AREA = "area";
    }

    public static final class GameInfoEntry implements BaseColumns {
        public static final String TABLE_NAME = "gameInfo";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_MONEY = "moneyBalance";
    }

    public static final class PlayerPropertiesEntry implements BaseColumns {
        public static final String TABLE_NAME = "playerProperties";
        public static final String COLUMN_HOUSES_NUMBER = "housesNumber";
        public static final String COLUMN_PLEDGED = "pledged";
        public static final String COLUMN_GAME_ID = "gameID";
    }
}
