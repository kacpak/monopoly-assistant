package pl.kacpak.monopolyassistant.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import pl.kacpak.monopolyassistant.App;
import pl.kacpak.monopolyassistant.R;
import pl.kacpak.monopolyassistant.data.DataContract.GameInfoEntry;
import pl.kacpak.monopolyassistant.data.DataContract.PlayerPropertiesEntry;
import pl.kacpak.monopolyassistant.data.DataContract.PropertyDataEntry;

/**
 * Created by Mateusz on 2014-08-08.
 */
public class MonopolyDbHelper extends SQLiteOpenHelper {

    private final String LOG_TAG = MonopolyDbHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "monopoly.db";
    private static Context mContext;

    static {
        mContext = App.getContext();
    }

    public MonopolyDbHelper() {
        super(mContext, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_PROPERTY_DATA_TABLE = "CREATE TABLE " + PropertyDataEntry.TABLE_NAME + " (" +
                PropertyDataEntry._ID + " INTEGER PRIMARY KEY, " +
                PropertyDataEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                PropertyDataEntry.COLUMN_DESCRIPTION + " TEXT DEFAULT NULL, " +
                PropertyDataEntry.COLUMN_PURCHASE_PRICE + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_HOUSE_PRICE + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_HOTEL_PRICE + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT0 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT1 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT2 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT3 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT4 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_RENT5 + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_MORTGAGE + " INTEGER NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_SPECIAL1 + " BOOLEAN NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_SPECIAL2 + " BOOLEAN NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_SPECIAL3 + " BOOLEAN NOT NULL DEFAULT 0, " +
                PropertyDataEntry.COLUMN_AREA + " INTEGER NOT NULL DEFAULT 0 " +
                " );";

        final String SQL_CREATE_GAME_INFO_TABLE = "CREATE TABLE " + GameInfoEntry.TABLE_NAME + " (" +
                GameInfoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                GameInfoEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                GameInfoEntry.COLUMN_MONEY + " INTEGER NOT NULL DEFAULT 1500 " +
                " );";

        final String SQL_CREATE_PLAYER_PROPERTIES_TABLE = "CREATE TABLE " + PlayerPropertiesEntry.TABLE_NAME + " (" +
                PlayerPropertiesEntry._ID + " INTEGER NOT NULL DEFAULT 0, " +
                PlayerPropertiesEntry.COLUMN_GAME_ID + " INTEGER NOT NULL DEFAULT 0, " +
                PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER + " INTEGER NOT NULL DEFAULT 0, " +
                PlayerPropertiesEntry.COLUMN_PLEDGED + " BOOLEAN NOT NULL DEFAULT 0, " +
                " FOREIGN KEY (" + PlayerPropertiesEntry._ID + ") REFERENCES " + PropertyDataEntry.TABLE_NAME + " (" + PropertyDataEntry._ID + ") ON DELETE CASCADE, " +
                " FOREIGN KEY (" + PlayerPropertiesEntry.COLUMN_GAME_ID + ") REFERENCES " + GameInfoEntry.TABLE_NAME + " (" + GameInfoEntry._ID + ") ON DELETE CASCADE, " +
                " UNIQUE (" + PlayerPropertiesEntry._ID + ", " + PlayerPropertiesEntry.COLUMN_GAME_ID + ") " +
                " );";

        db.execSQL(SQL_CREATE_PROPERTY_DATA_TABLE);
        db.execSQL(SQL_CREATE_GAME_INFO_TABLE);
        db.execSQL(SQL_CREATE_PLAYER_PROPERTIES_TABLE);

        fillPropertyDataTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + PropertyDataEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PlayerPropertiesEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + GameInfoEntry.TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.execSQL("PRAGMA foreign_keys=ON");
    }

    private void fillPropertyDataTable(SQLiteDatabase db) {
        final String JSONPropertyDataEntries = mContext.getString(R.string.JSON_PROPERTY_DATA_TABLE);

        try {
            JSONArray propertyEntriesArray = new JSONArray(JSONPropertyDataEntries);

            for (int i = 0; i < propertyEntriesArray.length(); i++) {
                JSONObject entry = propertyEntriesArray.getJSONObject(i);

                int id, placePrice, housePrice, hotelPrice, rent0, rent1, rent2, rent3, rent4, rent5, mortgage, special1, special2, special3, area;
                String name, description;

                id = entry.getInt("_ID");
                name = entry.getString("name");
                description = entry.getString("description");
                placePrice = entry.getInt("placePrice");
                housePrice = entry.getInt("housePrice");
                hotelPrice = entry.getInt("hotelPrice");
                rent0 = entry.getInt("rent0");
                rent1 = entry.getInt("rent1");
                rent2 = entry.getInt("rent2");
                rent3 = entry.getInt("rent3");
                rent4 = entry.getInt("rent4");
                rent5 = entry.getInt("rent5");
                mortgage = entry.getInt("mortgage");
                special1 = entry.getInt("special1");
                special2 = entry.getInt("special2");
                special3 = entry.getInt("special3");
                area = entry.getInt("area");

                ContentValues row = new ContentValues();
                row.put(PropertyDataEntry._ID, id);
                row.put(PropertyDataEntry.COLUMN_NAME, name);
                row.put(PropertyDataEntry.COLUMN_DESCRIPTION, description);
                row.put(PropertyDataEntry.COLUMN_PURCHASE_PRICE, placePrice);
                row.put(PropertyDataEntry.COLUMN_HOUSE_PRICE, housePrice);
                row.put(PropertyDataEntry.COLUMN_HOTEL_PRICE, hotelPrice);
                row.put(PropertyDataEntry.COLUMN_RENT0, rent0);
                row.put(PropertyDataEntry.COLUMN_RENT1, rent1);
                row.put(PropertyDataEntry.COLUMN_RENT2, rent2);
                row.put(PropertyDataEntry.COLUMN_RENT3, rent3);
                row.put(PropertyDataEntry.COLUMN_RENT4, rent4);
                row.put(PropertyDataEntry.COLUMN_RENT5, rent5);
                row.put(PropertyDataEntry.COLUMN_MORTGAGE, mortgage);
                row.put(PropertyDataEntry.COLUMN_SPECIAL1, special1);
                row.put(PropertyDataEntry.COLUMN_SPECIAL2, special2);
                row.put(PropertyDataEntry.COLUMN_SPECIAL3, special3);
                row.put(PropertyDataEntry.COLUMN_AREA, area);

                db.insert(PropertyDataEntry.TABLE_NAME, null, row);
            }

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }
}
