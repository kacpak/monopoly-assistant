package pl.kacpak.monopolyassistant.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.ArrayList;
import java.util.Arrays;

import pl.kacpak.monopolyassistant.data.DataContract.GameInfoEntry;
import pl.kacpak.monopolyassistant.data.DataContract.PlayerPropertiesEntry;
import pl.kacpak.monopolyassistant.data.DataContract.PropertyDataEntry;

/**
 * Created by Mateusz on 2014-08-13.
 */
public class GameManager {

    private static MonopolyDbHelper dbHelper;
    private long gameID;

    private final SQLiteQueryBuilder sPropertyDataQueryBuilder;                              // Używane do pobierania danych tabeli PropertyData
    private final SQLiteQueryBuilder sGameInfoQueryBuilder;                                  // Używane do pobierania danych o pieniądzach gracza
    private final SQLiteQueryBuilder sPlayerPropertiesQueryBuilder;                          // Używane do pobierania danych o wykupionych posiadłościach gracza

    static {
        dbHelper = new MonopolyDbHelper();
    }

    public GameManager(long gameID) {
        this.gameID = gameID;

        sPropertyDataQueryBuilder = new SQLiteQueryBuilder();
        sPropertyDataQueryBuilder.setTables(PropertyDataEntry.TABLE_NAME);

        sGameInfoQueryBuilder = new SQLiteQueryBuilder();
        sGameInfoQueryBuilder.setTables(GameInfoEntry.TABLE_NAME);
        sGameInfoQueryBuilder.appendWhere(GameInfoEntry._ID + " = " + gameID);

        sPlayerPropertiesQueryBuilder = new SQLiteQueryBuilder();
        sPlayerPropertiesQueryBuilder.setTables(PlayerPropertiesEntry.TABLE_NAME + " NATURAL JOIN " + PropertyDataEntry.TABLE_NAME);
        sPlayerPropertiesQueryBuilder.appendWhere(PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID);
    }

    public static long createNewGame(String gameName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DataContract.GameInfoEntry.COLUMN_NAME, gameName);
        return db.insert(DataContract.GameInfoEntry.TABLE_NAME, null, cv);
    }

    public static Cursor getSavedGamesCursor() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        return db.query(GameInfoEntry.TABLE_NAME,
                new String[] { GameInfoEntry._ID, GameInfoEntry.COLUMN_NAME },
                null, null, null, null, null);
    }

    public static void deleteGame(long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(GameInfoEntry.TABLE_NAME,
                GameInfoEntry._ID + " = " + id,
                null);
    }

    public int getPlayerMoney() {
        Cursor cursor = sGameInfoQueryBuilder.query(dbHelper.getReadableDatabase(),
                new String[]{ GameInfoEntry.COLUMN_MONEY }, null, null, null, null, null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public void setPlayerMoney(int money) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GameInfoEntry.COLUMN_MONEY, money);
        dbHelper.getWritableDatabase().update(GameInfoEntry.TABLE_NAME,
                contentValues,
                GameInfoEntry._ID + " = " + gameID,
                null);
    }

    public void addPlayerMoney(int money) {
        dbHelper.getWritableDatabase().execSQL("UPDATE " + GameInfoEntry.TABLE_NAME +
                " SET " + GameInfoEntry.COLUMN_MONEY + " = " + GameInfoEntry.COLUMN_MONEY + (money > 0 ? " + " : " - ") + Math.abs(money) +
                " WHERE " + GameInfoEntry._ID + " = " + gameID);
    }

    public void addProperty(long id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PlayerPropertiesEntry._ID, id);
        contentValues.put(PlayerPropertiesEntry.COLUMN_GAME_ID, gameID);

        dbHelper.getWritableDatabase().insert(PlayerPropertiesEntry.TABLE_NAME, null, contentValues);
    }

    public ArrayList<PlayerProperty> getNotOwnedProperties() {
        Cursor cursor = dbHelper.getReadableDatabase().query(
                PropertyDataEntry.TABLE_NAME + " AS `all` " +
                        "LEFT JOIN " + PlayerPropertiesEntry.TABLE_NAME + " AS `owned` " +
                        "ON (`all`." + PropertyDataEntry._ID + " = `owned`." + PlayerPropertiesEntry._ID + " AND `owned`." + PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID + ")",
                new String[] { "`all`." + PropertyDataEntry._ID, "`all`." + PropertyDataEntry.COLUMN_NAME, "`all`." + PropertyDataEntry.COLUMN_PURCHASE_PRICE },
                "`owned`." + PlayerPropertiesEntry._ID + " IS NULL",
                null, null, null, null
        );

        if (cursor == null)
            return null;

        return cursorToPlayerPropertyArrayList(cursor);
    }

    public PlayerProperty getPlayerProperty(long id) {
        Cursor cursor = sPlayerPropertiesQueryBuilder.query(dbHelper.getReadableDatabase(),
                null,
                PlayerPropertiesEntry._ID + " = " + id,
                null, null, null, null
        );

        if (cursor == null)
            return null;

        return cursorToPlayerPropertyArrayList(cursor).get(0);
    }

    public ArrayList<PlayerProperty> getPlayerProperties() {
        String[] columns = {
                PropertyDataEntry._ID,                          // 0
                PropertyDataEntry.COLUMN_NAME,                  // 1
                PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER,     // 2
                PropertyDataEntry.COLUMN_RENT0,                 // 3
                PropertyDataEntry.COLUMN_RENT1,                 // 4
                PropertyDataEntry.COLUMN_RENT2,                 // 5
                PropertyDataEntry.COLUMN_RENT3,                 // 6
                PropertyDataEntry.COLUMN_RENT4,                 // 7
                PropertyDataEntry.COLUMN_RENT5,                 // 8
                PropertyDataEntry.COLUMN_SPECIAL1,              // 9
                PropertyDataEntry.COLUMN_SPECIAL2,              // 10
                PlayerPropertiesEntry.COLUMN_PLEDGED,           // 11
                PropertyDataEntry.COLUMN_AREA                   // 12
        };

        Cursor cursor = sPlayerPropertiesQueryBuilder.query(dbHelper.getReadableDatabase(),
                columns,                        // Kolumny do pobrania
                null,                           // Zapytanie WHERE, ze zmiennymi wartościami jako ?
                null,                           // Tablica wartości do WHERE
                null,                           // Kolumny do grupowania
                null,                           // Kolumny do filtrowania po grupach wierszy
                PropertyDataEntry._ID + " ASC"  // Kolejność sortowania
        );

        if (cursor == null)
            return null;

        return cursorToPlayerPropertyArrayList(cursor);
    }

    public ArrayList<PlayerProperty> cursorToPlayerPropertyArrayList(Cursor cursor) {
        ArrayList<PlayerProperty> properties = new ArrayList<PlayerProperty>();
        ArrayList<String> columnNames = new ArrayList<String>(Arrays.asList(cursor.getColumnNames()));

        int i = 0;
        while (cursor.moveToNext()) {
            PlayerProperty property = new PlayerProperty();
            property.rent = new int[6];

            for (int j = 0; j < columnNames.size(); j++) {
                String columnName = columnNames.get(j);

                if (columnName.equals(PropertyDataEntry._ID))
                    property.id = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_NAME))
                    property.name = cursor.getString(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_DESCRIPTION))
                    property.description = cursor.getString(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_SPECIAL1))
                    property.isSpecial1 = cursor.getInt(j) == 1;

                else if (columnName.equals(PropertyDataEntry.COLUMN_SPECIAL2))
                    property.isSpecial2 = cursor.getInt(j) == 1;

                else if (columnName.equals(PropertyDataEntry.COLUMN_SPECIAL3))
                    property.isSpecial3 = cursor.getInt(j) == 1;

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT0))
                    property.rent[0] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT1))
                    property.rent[1] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT2))
                    property.rent[2] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT3))
                    property.rent[3] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT4))
                    property.rent[4] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_RENT5))
                    property.rent[5] = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_PURCHASE_PRICE))
                    property.placePrice = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_HOUSE_PRICE))
                    property.housePrice = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_HOTEL_PRICE))
                    property.hotelPrice = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_MORTGAGE))
                    property.mortgage = cursor.getInt(j);

                else if (columnName.equals(PropertyDataEntry.COLUMN_AREA))
                    property.area = cursor.getInt(j);

                else if (columnName.equals(PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER))
                    property.housesNumber = cursor.getInt(j);

                else if (columnName.equals(PlayerPropertiesEntry.COLUMN_PLEDGED))
                    property.isPledged = cursor.getInt(j) == 1;
            }

            if (columnNames.contains(PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER)) {
                // Jeśli posiadłość z kategorii SPECIAL1, to ustaw ilość domów na ilość posiadanych posiadłości SPECIAL1
                if (property.isSpecial1) {
                    property.housesNumber = (int) DatabaseUtils.queryNumEntries(dbHelper.getReadableDatabase(),
                            PlayerPropertiesEntry.TABLE_NAME + " NATURAL JOIN " + PropertyDataEntry.TABLE_NAME,
                            PropertyDataEntry.COLUMN_SPECIAL1 + " = 1 AND " + PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID,
                            null);
                }

                // Ustaw cenę za przejście przez posiadłość
                property.passingPrice = property.rent[property.housesNumber];

                // Jeśli posiadamy w danym regionie wszystkie posiadłości to zyski z niezabudowanych działek się podwajają
                if (property.housesNumber == 0 && !property.isSpecial1) {
                    String whereStatement = PropertyDataEntry.COLUMN_AREA + " = " + property.area;

                    long propertiesInArea = DatabaseUtils.queryNumEntries(dbHelper.getReadableDatabase(),
                            PropertyDataEntry.TABLE_NAME,
                            whereStatement, null);

                    long ownedPropertiesInArea = DatabaseUtils.queryNumEntries(dbHelper.getReadableDatabase(),
                            PlayerPropertiesEntry.TABLE_NAME + " NATURAL JOIN " + PropertyDataEntry.TABLE_NAME,
                            PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID + " AND " + whereStatement, null);

                    if (propertiesInArea == ownedPropertiesInArea)
                        property.passingPrice *= 2;
                }
            }

            properties.add(property);
            i++;
        }
        return properties;
    }

    public void changePlayerPropertyHousesNumber(long propertyId, int housesNumber) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PlayerPropertiesEntry.COLUMN_HOUSES_NUMBER, housesNumber);

        db.update(PlayerPropertiesEntry.TABLE_NAME,
                contentValues,
                PlayerPropertiesEntry._ID + " = " + propertyId + " AND " + PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID,
                null);
    }

    public void changePlayerPropertyMortgageStatus(long propertyId, boolean status) {
        int mortgage = (status) ? 1 : 0;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(PlayerPropertiesEntry.COLUMN_PLEDGED, mortgage);

        db.update(PlayerPropertiesEntry.TABLE_NAME,
                contentValues,
                PlayerPropertiesEntry._ID + " = " + propertyId + " AND " + PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID,
                null);
    }

    public void deletePlayerProperty(long propertyId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(PlayerPropertiesEntry.TABLE_NAME,
                PlayerPropertiesEntry._ID + " = " + propertyId + " AND " + PlayerPropertiesEntry.COLUMN_GAME_ID + " = " + gameID,
                null);
    }

    public class PlayerProperty extends Property {
        public int housesNumber;
        public int passingPrice;
        public boolean isPledged;

        public PlayerProperty() {
            super();
        }

        public PlayerProperty(long id, String name) {
            super(id, name);
        }

        public PlayerProperty(long id, String name, int housesNumber, int passingPrice, boolean isPledged, boolean isSpecial1, boolean isSpecial2) {
            super(id, name);
            this.housesNumber = housesNumber;
            this.passingPrice = passingPrice;
            this.isPledged = isPledged;
            this.isSpecial1 = isSpecial1;
            this.isSpecial2 = isSpecial2;
        }
    }

    public class Property {
        public long id;
        public String name;
        public String description;
        public boolean isSpecial1;
        public boolean isSpecial2;
        public boolean isSpecial3;
        public int rent[];
        public int area;
        public int placePrice;
        public int housePrice;
        public int hotelPrice;
        public int mortgage;

        public Property() {
            this.id = 0;
            this.name = null;
            this.description = null;
            this.isSpecial1 = this.isSpecial2 = this.isSpecial3 = false;
            this.rent = null;
            this.area = 0;
            this.placePrice = 0;
            this.housePrice = 0;
            this.hotelPrice = 0;
            this.mortgage = 0;
        }

        public Property(long id, String name) {
            this();
            this.id = id;
            this.name = name;
        }

        public Property(long id, String name, String description, int[] rent, int area, int placePrice, int housePrice, int hotelPrice, int mortgage) {
            this(id, name);
            this.description = description;
            this.rent = rent;
            this.area = area;
            this.placePrice = placePrice;
            this.housePrice = housePrice;
            this.hotelPrice = hotelPrice;
            this.mortgage = mortgage;
        }
    }

}
