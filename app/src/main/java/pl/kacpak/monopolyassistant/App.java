package pl.kacpak.monopolyassistant;

import android.app.Application;
import android.content.Context;

/**
 * Created by Mateusz on 2014-08-21.
 */
public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}