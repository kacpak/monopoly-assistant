package pl.kacpak.monopolyassistant;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;

import pl.kacpak.monopolyassistant.data.DataContract;

public class MainMenuActivity extends Activity implements MainMenuFragment.MainMenuCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.main_activity_container, new MainMenuFragment())
                    .commit();
        }
    }

    @Override
    public void onFragmentActionRequest(String data) {
        if (data == MainMenuFragment.NEW_GAME_CALL) {
            DialogFragment dialogFragment = new MainMenuDialogFragmentNewGame();
            dialogFragment.show(getFragmentManager(), "dialog");

        } else if (data == MainMenuFragment.LOAD_GAME_CALL) {
            DialogFragment dialogFragment = new MainMenuDialogFragmentLoadGame();
            dialogFragment.show(getFragmentManager(), "dialog");
        }
    }

    @Override
    public void onGameLoadId(long id) {
        if (id <= 0) return;
        Intent intent = new Intent(this, AssistantActivity.class);
        intent.putExtra(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, id);
        startActivity(intent);
    }
}
