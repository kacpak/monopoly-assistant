package pl.kacpak.monopolyassistant;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import pl.kacpak.monopolyassistant.data.DataContract;
import pl.kacpak.monopolyassistant.data.GameManager;

/**
 * Created by Mateusz on 2014-08-21.
 */
public class AssistantDialogFragmentBalanceChange extends DialogFragment {

    GameManager gameManager;
    long gameID;
    int quickChangeValue;
    Button quickChange1, quickChange5, quickChange10, quickChange50, quickChange100, quickChange500;
    int defaultTextColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID)) {
            gameID = args.getLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID);
        }
        gameManager = new GameManager(gameID);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assistant_balance_change, container, false);

        final EditText balanceEditText = (EditText) view.findViewById(R.id.balance);
        balanceEditText.setText("" + gameManager.getPlayerMoney());

        final Button acceptChangeButton = (Button) view.findViewById(R.id.balanceChange_accept);
        acceptChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(balanceEditText.getText().toString());
                gameManager.setPlayerMoney(value);
                ((AssistantFragment.AssistantCallback) getActivity()).updateBalance();
                dismiss();
            }
        });

        quickChange1 = (Button) view.findViewById(R.id.balanceChange_1);
        quickChange1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(1);
            }
        });
        quickChange5 = (Button) view.findViewById(R.id.balanceChange_5);
        quickChange5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(5);
            }
        });
        quickChange10 = (Button) view.findViewById(R.id.balanceChange_10);
        quickChange10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(10);
            }
        });
        quickChange50 = (Button) view.findViewById(R.id.balanceChange_50);
        quickChange50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(50);
            }
        });
        quickChange100 = (Button) view.findViewById(R.id.balanceChange_100);
        quickChange100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(100);
            }
        });
        quickChange500 = (Button) view.findViewById(R.id.balanceChange_500);
        quickChange500.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifyQuickChangeValue(500);
            }
        });

        defaultTextColor = quickChange1.getTextColors().getDefaultColor();

        modifyQuickChangeValue(100);

        final Button quickChangePlus = (Button) view.findViewById(R.id.balanceChange_plus);
        quickChangePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int money = Integer.parseInt(balanceEditText.getText().toString()) + quickChangeValue;
                balanceEditText.setText("" + money);
            }
        });

        final Button quickChangeMinus = (Button) view.findViewById(R.id.balanceChange_minus);
        quickChangeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int money = Integer.parseInt(balanceEditText.getText().toString()) - quickChangeValue;
                balanceEditText.setText("" + money);
            }
        });

        return view;
    }

    private void modifyQuickChangeValue(int value) {
        quickChangeValue = value;
        quickChange1.setTextColor(defaultTextColor);
        quickChange5.setTextColor(defaultTextColor);
        quickChange10.setTextColor(defaultTextColor);
        quickChange50.setTextColor(defaultTextColor);
        quickChange100.setTextColor(defaultTextColor);
        quickChange500.setTextColor(defaultTextColor);

        int activatedColor = getResources().getColor(R.color.main_dark);

        switch (value) {
            case 1:
                quickChange1.setTextColor(activatedColor); break;
            case 5:
                quickChange5.setTextColor(activatedColor); break;
            case 10:
                quickChange10.setTextColor(activatedColor); break;
            case 50:
                quickChange50.setTextColor(activatedColor); break;
            case 100:
                quickChange100.setTextColor(activatedColor); break;
            case 500:
                quickChange500.setTextColor(activatedColor); break;
        }
    }

}

