package pl.kacpak.monopolyassistant;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;

import pl.kacpak.monopolyassistant.data.GameManager;

/**
 * Created by Mateusz on 2014-08-15.
 */
public class MainMenuDialogFragmentNewGame extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu_new_game, container, false);

        String todayDate = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date());

        final EditText newGameName = (EditText) view.findViewById(R.id.new_game_dialog_name);
        newGameName.setText(todayDate);

        Button createButton = (Button) view.findViewById(R.id.new_game_dialog_create);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gameName = newGameName.getText().toString();

                long id = GameManager.createNewGame(gameName);

                ((MainMenuFragment.MainMenuCallback) getActivity()).onGameLoadId(id);
                dismiss();
            }
        });

        return view;
    }
}
