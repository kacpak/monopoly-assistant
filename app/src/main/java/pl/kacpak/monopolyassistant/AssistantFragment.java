package pl.kacpak.monopolyassistant;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import pl.kacpak.monopolyassistant.data.DataContract;
import pl.kacpak.monopolyassistant.data.GameManager;

/**
 * Created by Mateusz on 2014-08-01.
 */
public class AssistantFragment extends Fragment {

    private ListView mListView;
    private Button mBalanceButton;
    private AssistantPlayerPropertiesAdapter mPlayerPropertiesAdapter;
    private GameManager mGameManager;
    private long mGameId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle args = getArguments();
        if (args != null && args.containsKey(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID))
            mGameId = args.getLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID);

        mGameManager = new GameManager(mGameId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.game_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete_game: {
                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.game_delete_confirmation_question)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                GameManager.deleteGame(mGameId);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
                return true;
            }
            default: break;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_assistant, container, false);

        mPlayerPropertiesAdapter = new AssistantPlayerPropertiesAdapter(getActivity(), mGameManager.getPlayerProperties());

        mListView = (ListView) rootView.findViewById(R.id.properties_listview);
        mListView.setAdapter(mPlayerPropertiesAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long propertyId = Long.parseLong(((TextView) view.findViewById(R.id.list_item_id)).getText().toString());

                Bundle args = new Bundle();
                args.putLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, mGameId);
                args.putLong(DataContract.PlayerPropertiesEntry._ID, propertyId);

                DialogFragment fragment = new AssistantDialogFragmentPropertyDetail();
                fragment.setArguments(args);
                fragment.show(getFragmentManager(), "dialog");
            }
        });

        Button addPropertyButton = (Button) rootView.findViewById(R.id.list_item_add_property_button);
        addPropertyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, mGameId);

                DialogFragment fragment = new AssistantDialogFragmentPropertyAdd();
                fragment.setArguments(args);
                fragment.show(getFragmentManager(), "dialog");
            }
        });

        mBalanceButton = (Button) rootView.findViewById(R.id.balance);
        mBalanceButton.setText(formatBalance(mGameManager.getPlayerMoney()));
        mBalanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, mGameId);

                DialogFragment fragment = new AssistantDialogFragmentBalanceChange();
                fragment.setArguments(args);
                fragment.show(getFragmentManager(), "dialog");
            }
        });

        return rootView;
    }

    public void updateOwnedProperties() {
        mPlayerPropertiesAdapter.clear();
        mPlayerPropertiesAdapter.addAll(mGameManager.getPlayerProperties());
        mPlayerPropertiesAdapter.notifyDataSetChanged();
    }

    public void updateBalance() {
        mBalanceButton.setText(formatBalance(mGameManager.getPlayerMoney()));
    }

    private String formatBalance(int money) {
        if (money >= 0) return String.format(getString(R.string.assistant_money_format), money);

        money = Math.abs(money);
        return "- " + String.format(getString(R.string.assistant_money_format), money);
    }

    public interface AssistantCallback {
        public void updateOwnedProperties();
        public void updateBalance();
    }
}
