package pl.kacpak.monopolyassistant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import pl.kacpak.monopolyassistant.data.DataContract;

public class AssistantActivity extends Activity implements AssistantFragment.AssistantCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        Intent intent = getIntent();
        long gameID = intent.getLongExtra(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, 0);

        if (savedInstanceState == null) {
            Bundle args = new Bundle();
            args.putLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID, gameID);

            AssistantFragment assistantFragment = new AssistantFragment();
            assistantFragment.setArguments(args);

            getFragmentManager().beginTransaction()
                    .add(R.id.main_activity_container, assistantFragment)
                    .commit();
        }
    }

    @Override
    public void updateOwnedProperties() {
        ((AssistantFragment) getFragmentManager().findFragmentById(R.id.main_activity_container)).updateOwnedProperties();
    }

    @Override
    public void updateBalance() {
        ((AssistantFragment) getFragmentManager().findFragmentById(R.id.main_activity_container)).updateBalance();
    }
}
