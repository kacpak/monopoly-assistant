package pl.kacpak.monopolyassistant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.kacpak.monopolyassistant.data.DataContract;
import pl.kacpak.monopolyassistant.data.GameManager;
import pl.kacpak.monopolyassistant.data.GameManager.PlayerProperty;

/**
 * Created by Mateusz on 2014-08-21.
 */
public class AssistantDialogFragmentPropertyAdd extends DialogFragment {

    GameManager gameManager;
    long gameID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID)) {
            gameID = args.getLong(DataContract.PlayerPropertiesEntry.COLUMN_GAME_ID);
        }
        gameManager = new GameManager(gameID);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assistant_property_add, container, false);

        NoDetailPropertyAdapter adapter = new NoDetailPropertyAdapter(getActivity(), gameManager.getNotOwnedProperties());

        ListView listView = (ListView) view.findViewById(R.id.add_property_listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final long propertyId = Long.parseLong(((TextView) view.findViewById(R.id.list_item_id)).getText().toString());
                gameManager.addProperty(propertyId);
                ((AssistantFragment.AssistantCallback) getActivity()).updateOwnedProperties();
                getDialog().hide();

                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.property_add_payment_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                gameManager.addPlayerMoney(-gameManager.getPlayerProperty(propertyId).placePrice);
                                ((AssistantFragment.AssistantCallback) getActivity()).updateBalance();
                                dismiss();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dismiss();
                            }
                        })
                        .show();

            }
        });

        return view;
    }

    private class NoDetailPropertyAdapter extends ArrayAdapter<PlayerProperty> {

        private Context mContext;
        private ArrayList<PlayerProperty> mValues;

        public NoDetailPropertyAdapter(Context context, ArrayList<PlayerProperty> values) {
            super(context, R.layout.list_item_property_buy, values);
            mContext = context;
            mValues = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = (convertView == null) ? LayoutInflater.from(mContext).inflate(R.layout.list_item_property_buy, parent, false) : convertView;

            PlayerProperty playerProperty = mValues.get(position);

            TextView id = (TextView) rowView.findViewById(R.id.list_item_id);
            id.setText(String.valueOf(playerProperty.id));

            TextView name = (TextView) rowView.findViewById(R.id.list_item_name);
            name.setText(playerProperty.name);

            TextView price = (TextView) rowView.findViewById(R.id.list_item_price);
            price.setText(String.format(getActivity().getString(R.string.assistant_money_format), playerProperty.placePrice));

            return rowView;
        }
    }
}

