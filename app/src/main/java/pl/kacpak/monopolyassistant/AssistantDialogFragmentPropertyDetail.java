package pl.kacpak.monopolyassistant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import pl.kacpak.monopolyassistant.data.DataContract.PlayerPropertiesEntry;
import pl.kacpak.monopolyassistant.data.GameManager;
import pl.kacpak.monopolyassistant.data.GameManager.PlayerProperty;

/**
 * Created by Mateusz on 2014-08-16.
 */
public class AssistantDialogFragmentPropertyDetail extends DialogFragment {

    GameManager gameManager;
    PlayerProperty playerProperty;
    long gameID, propertyID;
    TextView mPropertyHousesNumber;
    Button mPropertySell, mPropertyPawn, mPropertyHousesPlus, mPropertyHousesMinus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(PlayerPropertiesEntry.COLUMN_GAME_ID) &&  args.containsKey(PlayerPropertiesEntry._ID)) {
            propertyID = args.getLong(PlayerPropertiesEntry._ID);
            gameID = args.getLong(PlayerPropertiesEntry.COLUMN_GAME_ID);
        }
        gameManager = new GameManager(gameID);
        playerProperty = gameManager.getPlayerProperty(propertyID);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        if (playerProperty.isSpecial1)
            view = inflater.inflate(R.layout.fragment_assistant_property_detail_special1, container, false);
        else if (playerProperty.isSpecial2)
            view = inflater.inflate(R.layout.fragment_assistant_property_detail_special2, container, false);
        else
            view = inflater.inflate(R.layout.fragment_assistant_property_detail, container, false);

        final TextView propertyName = (TextView) view.findViewById(R.id.property_detail_name);
        propertyName.setText(playerProperty.name);

        mPropertySell = (Button) view.findViewById(R.id.property_detail_sell);
        mPropertySell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.property_detail_delete_confirmation_question)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                gameManager.deletePlayerProperty(propertyID);
                                ((AssistantFragment.AssistantCallback) getActivity()).updateOwnedProperties();
                                dismiss();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
            }
        });

        mPropertyPawn = (Button) view.findViewById(R.id.property_detail_pawn);
        mPropertyPawn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerProperty.isPledged) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.property_detail_mortgage_end_confirmation_question)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    playerProperty.isPledged = !playerProperty.isPledged;
                                    gameManager.changePlayerPropertyMortgageStatus(propertyID, playerProperty.isPledged);

                                    double returnedValue = -1.1 * playerProperty.mortgage;

                                    gameManager.addPlayerMoney((int)returnedValue);
                                    ((AssistantFragment.AssistantCallback) getActivity()).updateOwnedProperties();
                                    ((AssistantFragment.AssistantCallback) getActivity()).updateBalance();

                                    updateDetailInterface();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();

                } else {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(R.string.property_detail_mortgage_confirmation_question)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    playerProperty.isPledged = !playerProperty.isPledged;
                                    gameManager.changePlayerPropertyMortgageStatus(propertyID, playerProperty.isPledged);

                                    gameManager.addPlayerMoney(playerProperty.mortgage);
                                    ((AssistantFragment.AssistantCallback) getActivity()).updateBalance();
                                    ((AssistantFragment.AssistantCallback) getActivity()).updateOwnedProperties();

                                    if (!playerProperty.isSpecial1 && !playerProperty.isSpecial2)
                                        changeHousesNumber(0);

                                    updateDetailInterface();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                }
            }
        });

        if (playerProperty.isSpecial1)
            propertySpecial1ViewSetup(view);
        else if (playerProperty.isSpecial2)
            propertySpecial2ViewSetup(view);
        else
            propertyViewSetup(view);

        updateDetailInterface();

        return view;
    }

    private void updateDetailInterface() {
        mPropertyPawn.setText(playerProperty.isPledged ? R.string.property_detail_redeem : R.string.property_detail_pawn);

        if (playerProperty.isSpecial1 || playerProperty.isSpecial2 || playerProperty.isSpecial3) return;

        if (playerProperty.housesNumber > 0)
            mPropertySell.setEnabled(false);
        else
            mPropertySell.setEnabled(true);

        if (playerProperty.isPledged) {
            mPropertyHousesMinus.setEnabled(false);
            mPropertyHousesPlus.setEnabled(false);

        } else {
            mPropertyHousesMinus.setEnabled(true);
            mPropertyHousesPlus.setEnabled(true);

            if (playerProperty.housesNumber > 0)
                mPropertyPawn.setEnabled(false);
            else
                mPropertyPawn.setEnabled(true);
        }
    }

    private void propertyViewSetup(View view) {
        String moneyFormat = getString(R.string.assistant_money_format);

        final TextView propertyHouses0 = (TextView) view.findViewById(R.id.property_detail_rent_h0);
        propertyHouses0.setText(String.format(moneyFormat, playerProperty.rent[0]));

        final TextView propertyHouses1 = (TextView) view.findViewById(R.id.property_detail_rent_h1);
        propertyHouses1.setText(String.format(moneyFormat, playerProperty.rent[1]));

        final TextView propertyHouses2 = (TextView) view.findViewById(R.id.property_detail_rent_h2);
        propertyHouses2.setText(String.format(moneyFormat, playerProperty.rent[2]));

        final TextView propertyHouses3 = (TextView) view.findViewById(R.id.property_detail_rent_h3);
        propertyHouses3.setText(String.format(moneyFormat, playerProperty.rent[3]));

        final TextView propertyHouses4 = (TextView) view.findViewById(R.id.property_detail_rent_h4);
        propertyHouses4.setText(String.format(moneyFormat, playerProperty.rent[4]));

        final TextView propertyHouses5 = (TextView) view.findViewById(R.id.property_detail_rent_h5);
        propertyHouses5.setText(String.format(moneyFormat, playerProperty.rent[5]));

        final TextView propertyMortgage = (TextView) view.findViewById(R.id.property_detail_mortgage);
        propertyMortgage.setText(String.format(moneyFormat, playerProperty.mortgage));

        final TextView propertyHousePrice = (TextView) view.findViewById(R.id.property_detail_house_price);
        propertyHousePrice.setText(String.format(moneyFormat, playerProperty.housePrice));

        final TextView propertyHotelPrice = (TextView) view.findViewById(R.id.property_detail_hotel_price);
        String hotelPrice = String.format(getString(R.string.property_detail_hotel_price_plus_houses), String.format(moneyFormat, playerProperty.housePrice));
        propertyHotelPrice.setText(hotelPrice);

        mPropertyHousesMinus = (Button) view.findViewById(R.id.property_detail_house_minus);
        mPropertyHousesMinus.setOnClickListener(new HouseNumberChangeOnClickListener());

        mPropertyHousesPlus = (Button) view.findViewById(R.id.property_detail_house_plus);
        mPropertyHousesPlus.setOnClickListener(new HouseNumberChangeOnClickListener());

        mPropertyHousesNumber = (TextView) view.findViewById(R.id.property_detail_house_number);
        mPropertyHousesNumber.setText("" + playerProperty.housesNumber);
    }

    private void propertySpecial1ViewSetup(View view) {
        String moneyFormat = getString(R.string.assistant_money_format);

        TextView propertyHouses1 = (TextView) view.findViewById(R.id.property_detail_rent_h1);
        propertyHouses1.setText(String.format(moneyFormat, playerProperty.rent[1]));

        TextView propertyHouses2 = (TextView) view.findViewById(R.id.property_detail_rent_h2);
        propertyHouses2.setText(String.format(moneyFormat, playerProperty.rent[2]));

        TextView propertyHouses3 = (TextView) view.findViewById(R.id.property_detail_rent_h3);
        propertyHouses3.setText(String.format(moneyFormat, playerProperty.rent[3]));

        TextView propertyHouses4 = (TextView) view.findViewById(R.id.property_detail_rent_h4);
        propertyHouses4.setText(String.format(moneyFormat, playerProperty.rent[4]));

        TextView propertyMortgage = (TextView) view.findViewById(R.id.property_detail_mortgage);
        propertyMortgage.setText(String.format(moneyFormat, playerProperty.mortgage));

    }

    private void propertySpecial2ViewSetup(View view) {
        TextView propertyDesc = (TextView) view.findViewById(R.id.property_detail_description);
        propertyDesc.setText(playerProperty.description);

        String moneyFormat = getString(R.string.assistant_money_format);
        TextView propertyMortgage = (TextView) view.findViewById(R.id.property_detail_mortgage);
        propertyMortgage.setText(String.format(moneyFormat, playerProperty.mortgage));
    }

    private void changeHousesNumber(int value) {
        playerProperty.housesNumber = value;
        mPropertyHousesNumber.setText("" + playerProperty.housesNumber);

        gameManager.changePlayerPropertyHousesNumber(playerProperty.id, playerProperty.housesNumber);
        ((AssistantFragment.AssistantCallback) getActivity()).updateOwnedProperties();
    }

    private class HouseNumberChangeOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int changeValue = 0;

            switch (v.getId()) {
                case R.id.property_detail_house_minus: {
                    changeValue--;
                    break;
                }
                case R.id.property_detail_house_plus: {
                    changeValue++;
                    break;
                }
                default: break;
            }

            if (playerProperty.housesNumber <= 0 && changeValue < 0
                    || playerProperty.housesNumber >= 5 && changeValue > 0) return;

            changeHousesNumber(playerProperty.housesNumber + changeValue);

            updateDetailInterface();
        }
    }
}
