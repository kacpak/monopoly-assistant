package pl.kacpak.monopolyassistant;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Mateusz on 2014-08-01.
 */
public class MainMenuFragment extends Fragment {

    public static final String NEW_GAME_CALL = "new_game";
    public static final String LOAD_GAME_CALL = "load_game";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_menu, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().findViewById(R.id.new_game_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainMenuCallback) getActivity()).onFragmentActionRequest(NEW_GAME_CALL);
            }
        });
        getView().findViewById(R.id.load_game_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainMenuCallback) getActivity()).onFragmentActionRequest(LOAD_GAME_CALL);
            }
        });
    }

    public interface MainMenuCallback {
        public void onGameLoadId(long id);
        public void onFragmentActionRequest(String data);
    }
}


